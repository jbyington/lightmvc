<?php

define('SERVER_ROOT', dirname(__FILE__));

define('SITE_ADDRESS', 'http://'.$_SERVER['SERVER_NAME']);
define('SITE_NAME', 'LightMVC');

define('CLASS_PATH', SERVER_ROOT.'/Classes');
define('PUBLIC_PATH', SERVER_ROOT . '/Public');
define('ERROR_PATH', SERVER_ROOT . '/Errors')	;
define('PLUGIN_PATH', SERVER_ROOT . '/Plugins');
define('APP_PATH', SERVER_ROOT . '/Applications');
define('TEST_PATH', SERVER_ROOT . '/Tests');


$_PUT = array();
if( $_SERVER['REQUEST_METHOD'] == 'PUT' ) 
{ 
   parse_str(file_get_contents('php://input'), $_PUT);
}

$_DELETE = array();
if( $_SERVER['REQUEST_METHOD'] == 'DELETE' ) 
{ 
   parse_str(file_get_contents('php://input'), $_DELETE);
}



require CLASS_PATH . '/Light/Autoload.php';
Light_Autoload::init();
	
$mvc = new Light_Mvc();
echo $mvc->init( $_SERVER['REQUEST_URI'] );