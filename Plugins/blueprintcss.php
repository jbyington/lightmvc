<?php
	class blueprintcss extends Light_Controller_Plugin
	{	
		public function init()
		{
			$this->view()->add_css('/libraries/blueprint/screen.css', 'screen, projection');
			$this->view()->add_css('/libraries/blueprint/print.css', 'print');
			$this->view()->add_css('/libraries/blueprint/ie.css', 'screen, projection', 'lt IE 8');
		}	
	}