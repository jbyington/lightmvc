<?php
	class jquery_lightbox extends Light_Controller_Plugin
	{	
		public function init()
		{
			$this->view()->add_js('/libraries/jquery-lightbox/js/jquery.lightbox-0.5.min.js');
			$this->view()->add_css('/libraries/jquery-lightbox/css/jquery.lightbox-0.5.css','screen');
		}	
	}