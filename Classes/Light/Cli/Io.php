<?php

if( !defined('NL') )
{
	define('NL', chr(13) . chr(10) );	
}

class Light_Cli_Io
{	
	public static function in( $count = false )
	{
		if( !$count )
		{
			return fgets(STDIN);
		}
		else if( $count == 1 )
		{
			return trim( fgetc(STDIN) );
		}
		else
		{
			return fgets(STDIN, $count);
		}
	}
	
	public static function out( $string )
	{
		fwrite(STDOUT, $string); 
	}

	public static function err( $string )
	{
		fwrite(STDERR, $string); 
	}
}