<?php

if( !defined('NL') )
{
	define('NL', chr(13) . chr(10) );	
}

class Light_Cli_Tool
{
	public $args = '';

	protected $actions = array( 'add', 'create', 'delete' );
	protected $objects = array( 'controller', 'module', 'helper' );
	protected $name = '';

	public function __construct( $argv )
	{
		$this->args = Light_Cli_Args::clean( $argv );

		$action = array_shift($this->args);
	
		echo `clear`;
		$this->header();
		$this->$action($this->args);
		$this->footer();
	

	}

	public function header()
	{
		Light_Cli_Io::out(NL);
		Light_Cli_Io::out('  +============================================================================+' . NL);
		Light_Cli_Io::out('  |             LET THERE BE LIGHT - The LightMVC Code Generator               |' . NL);
		Light_Cli_Io::out('  +============================================================================+' . NL);
	}

	public function footer()
	{
		Light_Cli_Io::out(NL . NL);
	}

	public function __call( $method, $arguments )
	{
		Light_Cli_Io::out
		(
			  NL . "$method is an unknown action"
			. NL . 'Please use one of the following actions:'
			. NL . '* add'
			. NL . '* create'
			. NL . '* delete'
			#. NL . '* install'  #someday I hope to have a apt-get like repo!
			#. NL . '* uninstall' 
			. NL
		);
	}

	public function add( $args )
	{
		$type = !empty( $args ) ? array_shift( $args ) : false;
		$name   = !empty( $args ) ? array_shift( $args ) : false;
		$target = !empty( $args ) ? array_shift( $args ) : false;
		$path = !$target ? APPLICATION_PATH : MODULE_PATH . "/$target";

		if( !$type || $type == 'help')
		{
			Light_Cli_Io::out
			(
				NL .
				'   Must define what to add.' . NL .
				'   Valid types are:' . NL .
				'   * controller' . NL .
				'   * helper' . NL .
				'   * layout' . NL .
				'   * partial' . NL
			);
			return false;
		}

		if( !$name )
		{
			Light_Cli_Io::out( NL . "   Must supply a name for the $type you are adding." . NL );
			return false;
		}

		switch( $type )
		{
			case 'controller':
				$this->_create_controller( $path, $name );
				break;
			
			case 'helper':
				$this->_create_helper( $path, $name );
				break;
			
			case 'layout':			
				$this->_create_layout( $path, $name );
				break;
			
			case 'partial':
				$this->_create_partial( $path, $name );
				break;
		
		}
	}

	public function create( $args )
	{
		$type = !empty( $args ) ? array_shift( $args ) : false;
		$name   = !empty( $args ) ? array_shift( $args ) : false;
		$target = !empty( $args ) ? array_shift( $args ) : 'default';
		$path = APP_PATH . "/$target";

		if( !$type || $type == 'help')
		{
			Light_Cli_Io::out
			(
				  NL . '   Must define what to create.' 
				. NL . '   Valid types are:' 
				. NL . '   * application'
				. NL . '   * model' 
				. NL . '   * model-crud'
			);
			return false;
		}

		if( !$name )
		{
			Light_Cli_Io::out( NL . "   Must supply a name for the $type you are creating." . NL );
			return false;
		}

		switch( $type )
		{
			case 'application':
				$this->_create_application( APP_PATH, $name );
				break;

			case 'model':
				$this->_create_model( $path, $name );
				break;
		
			case 'module-crud':
				$this->_create_model_crud( MODULE_PATH, $name );
				break;
		
		}		
	}	

	public function delete( $args )
	{
		Light_Cli_Io::out
		(
			  NL . 'Delete command currently not implemented.'
			. NL . "Please locate $object manually and delete."
		);			
	}

	protected function _create_application( $path, $name )
	{
		$template = new Light_Cli_Template();
		Light_Directory::create( "$path/$name" );
		Light_File::touch( "$path/$name/config.php" );		
		Light_Directory::create( "$path/$name/controllers" );
		Light_Directory::create( "$path/$name/models" );
		Light_Directory::create( "$path/$name/views" );
		Light_Directory::create( "$path/$name/views/helpers" );
		Light_Directory::create( "$path/$name/views/partials" );
		Light_Directory::create( "$path/$name/views/layouts" );
		Light_Directory::create( "$path/$name/views/scripts" );

		$this->_create_controller( "$path/$name", 'index' );
	}

	protected function _create_model( $path, $name )
	{
		$template = new Light_Cli_Template();
		Light_File::touch( "$path/models/$name.php" );
		Light_File::write( "$path/models/$name.php", $template->model( $name ) );
	}

	protected function _create_model_crud( $path, $name )
	{
		$template = new Light_Cli_Template();
		Light_File::touch( "$path/models/$name.php" );
		Light_File::write( "$path/models/$name.php", $template->model_crud( $name ) );
	}

	protected function _create_controller( $path, $name )
	{
		$template = new Light_Cli_Template();
		Light_File::touch( "$path/controllers/$name.php" );
		Light_File::write( "$path/controllers/$name.php", $template->controller( $name ) );
		Light_Directory::create( "$path/views/scripts/$name" );
		Light_File::touch( "$path/views/scripts/$name/index.php" );
	}

	protected function _create_helper( $path, $name )
	{
		$template = new Light_Cli_Template();
		Light_Directory::create( "$path/views/helpers/" . str_replace( '_', '/', $name) );
		Light_File::write( "$path/views/helpers/" . str_replace( '_', '/', $name) . ".php", $template->helper( $name ) );
		Light_Directory::delete( "$path/views/helpers/" . str_replace( '_', '/', $name) );
	}

	protected function _create_layout( $path, $name )
	{
		$template = new Light_Cli_Template();
		Light_File::touch( "$path/views/layouts/$name.php" );
		Light_File::write( "$path/views/layouts/$name.php", $template->layout() );
	}

	protected function _create_partial( $path, $name )
	{
		$template = new Light_Cli_Template();
		Light_File::touch( "$path/views/partials/$name.php" );
	}

}