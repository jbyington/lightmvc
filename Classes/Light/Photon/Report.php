<?php

class Light_Photon_Report
{
	protected $num_success = 0;
	protected $num_failure = 0;
	
	protected $report_name;
	
	protected $report_start;
	protected $report_end;
	protected $total_time;
	
	protected $test_records = array();
	
	public function __construct()
	{	
		$this->test_records = array();
	}

	public function set_name( $name )
	{
		$this->report_name = $name;
	}
	
	public function record( $name, $start, $end, $result )
	{
		$test_record = array
		(
			'name' => $name,
			'start' => $start, 
			'end' => $end,
			'total' => $end - $start,
			'result' => $result
		);

		array_push( $this->test_records, $test_record);

		return $this->_talley( $result );
	}
	
	public function display()
	{
		Light_Cli_Io::out
		(
			'PHOTON: Light Unit' . NL . NL .
			'Class: ' . $this->report_name . NL .
			'Total Runtime: ' . number_format( $this->total_time, 2 ) . ' Seconds' . NL .
			$this->num_success . ' Successe(s)' . NL . 
			$this->num_failure . ' Failure(s): ' .NL . NL
		);
		foreach( $this->test_records as $record )
		{
			if( $record['result'] == false )
			{
				Light_Cli_Io::out( str_pad( $record['name'], 73, '.') . ' FAIL' .  NL );
			}
			else
			{
				Light_Cli_Io::out( str_pad( $record['name'], 73, '.') . ' PASS' .  NL );		
			}
		}

	}
	
	public function start()
	{
		$this->report_start = microtime(true);
	}
	
	public function end()
	{
		$this->report_end = microtime(true);
		$this->total_time = $this->report_end - $this->report_start;
	}
	
	# -------------------------------------------
	
	protected function _talley( $result = false )
	{
		if( $result )
		{
			return $this->_success();
		}
		else
		{
			return $this->_failure();
		}
	}
	
	protected function _success()
	{
		if( $this->num_success++ )
		{
			return true;
		}
		return false;
	}
	
	protected function _failure()
	{
		if( $this->num_failure++ )
		{
			return true;
		}
		return false;
	}
		
}