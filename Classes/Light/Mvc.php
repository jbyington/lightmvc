<?php

define('NL', chr(13) . chr(10) );

class Light_Mvc
{

	public function __construct()
	{
		@require APP_PATH . '/config.php';
	}

	protected static function _pre_dispatch()
	{
		Light_Session::start();

		#	Initialize the Helper Functions
		Light_Functions::init();
	}

	protected function _route( $request_uri )
	{
		#	Create an instance of the Controller class, and pass it the Route
		if( !isset($request_uri) )
		{
			$request_uri = $_SERVER['REQUEST_URI'];
		}
		$route = Light_Router::init( $request_uri );

		return $route;
	}

	public function init( $request_uri )
	{
		$this->_pre_dispatch();

		extract( $this->_route( $request_uri ) );

		return $this->dispatch( $module, $controller, $action, @$params );
	}

	protected function _get_controller_path( $module, $controller )
	{
		return APP_PATH . "/{$module}/controllers/{$controller}.php";
	}

	public function dispatch( $module = false, $controller = false, $action = false, $params = array() )
	{

		$controller_path = $this->_get_controller_path( $module, $controller );

		if( !file_exists($controller_path) )
		{
			$controller = '__call';
			$controller_path = $this->_get_controller_path( $module, $controller );
		}

		if( !file_exists($controller_path) )
		{
			$controller = '__call';
			$controller_path = $this->_get_controller_path( 'default', $controller );
		}

		if( !file_exists($controller_path) )
		{
			$app_controller = new Light_Controller();
			$response = $app_controller->_error('404');
		}
		else
		{
			include $controller_path;
			$controller_name = $controller . '_controller';
			$app_controller = new $controller_name();

			# check to see if the desired method exists
			if( ($action && !method_exists($app_controller, $action)) && !method_exists($app_controller, '__call') )
			{
				# the method you wanted didn't exist, so we go to the 404 method.
				$response = $app_controller->_error('404');
			}
			else
			{
				if( $action && !is_callable( array($app_controller, $action) ) )
				{
					# the method you wanted exists, but is not callable, so we go to the 403 method.
					$response = $app_controller->_error('403');
				}
				else
				{
					# If the method exists, call it. Pass in the params from Light_Router
					$response = $app_controller->_run( $action, $params );
				}
			}
		}

		return $response;
	}
}