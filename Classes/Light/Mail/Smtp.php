<?php

class Light_Mail_Smtp extends Light_Mail
{
	protected $auth	 = true;
	protected $port	 = '';
	protected $hosts	= '';
	protected $username = '';
	protected $password = '';

	public function send()
	{
		$this->mail = Mail::factory
		(
			'smtp',
			array
			(
				'host'	 => $this->get_hosts(),
				'port'	 => $this->get_port(),
				'auth'	 => $this->get_auth(),
				'username' => $this->get_username(),
				'password' => $this->get_password()
			)
		);

		$body = $this->mime->get();
		$headers = $this->mime->headers( $this->headers );

		return $this->mail->send( $this->get_to(), $headers, $body );
	}

	public function get_auth()
	{
		return $this->auth;
	}

	public function set_auth( $auth = true )
	{
		$this->auth = $auth;
		return $this;
	}

	public function get_hosts()
	{
		return $this->hosts;
	}

	public function set_hosts( $hosts )
	{
		$this->hosts = $hosts;
		return $this;
	}

	public function get_port()
	{
		return $this->port;
	}

	public function set_port( $port )
	{
		$this->port = $port;
		return $this;
	}

	public function get_username()
	{
		return $this->username;
	}

	public function set_username( $username )
	{
		$this->username = $username;
		return $this;
	}

	public function get_password()
	{
		return $this->password;
	}

	public function set_password( $password )
	{
		$this->password = $password;
		return $this;
	}
}
