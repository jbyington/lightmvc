<?php

class Light_Controller_Plugin
{
	public $_view = false;

	public function _before_call()
	{
		return; 
	}

	protected function init() 
	{ 
		return; 
	}

	public function _after_call() 
	{ 
		return; 
	}
	
	public function view()
	{
		if( !$this->_view )
		{
			$this->_view = Light_Registry::get('view');
		}
		return $this->_view;
	}

}