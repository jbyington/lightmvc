<?php

class Light_Controller_Action extends Light_Controller_Plugin
{
	protected $get;
	protected $post;
	protected $put;
	protected $delete;
	protected $request;
				
	#
	# Public Methods
	#
	
	public function __construct()
	{
		$this->_view = new Light_View();
		Light_Registry::set('view', $this->_view);

		global $_PUT, $_DELETE;

		$this->request = new Light_Controller_Param( $_REQUEST );
		$this->get = new Light_Controller_Param( $_GET );
		$this->post = new Light_Controller_Param( $_POST );
		$this->put = new Light_Controller_Param( $_PUT );
		$this->delete = new Light_Controller_Param( $_DELETE );
	}

	public function _run( $action, $params )
	{
		@require APP_PATH . '/' . Light_Router::$module . '/config.php';

		$this->_before_call();
		Light_Plugin::run('_before_call');
		
		$this->init();
		Light_Plugin::run('init');
		
		$this->$action();

		$this->_after_call();
		
		Light_Plugin::run('_after_call');
		
		return $this->view()->_display();
	}
	
	final public function _error( $error = '404' )
	{
		Light_Plugin::run('_before_call');		
		Light_Plugin::run('init');
		Light_Plugin::run('_after_call');
		return $this->view()->_display( $error );
	}
	
}