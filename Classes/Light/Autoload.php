<?php

class Light_Autoload
{

	public function __construct()
	{
		return;
	}

	public static function init()
	{
		#Order in which to call autoloaders
		spl_autoload_register( array( 'self', 'Load_From_Class_Folder' ), false );
		spl_autoload_register( array( 'self', 'Load_From_Application_Folder' ), false );
		spl_autoload_register( array( 'self', 'Load_From_Default_Folder' ), false );
	}

	public static function register( $callback, $prepend = false )
	{
		spl_autoload_register( $callback, false, $prepend );
	}

	#Load From Class Folder
	public static function Load_From_Class_Folder( $class_name )
	{
		$class_name = str_replace( '_', '/', $class_name );

		$require_path = CLASS_PATH . '/' . $class_name . '.php';

		if( file_exists($require_path) )
		{
			require_once $require_path;
			return true;
		}
		return false;
	}

	#Load From Default Application Folder
	public static function Load_From_Default_Folder( $class_name )
	{
		$class_name = str_replace( '_', '/', $class_name );

		$require_path = APP_PATH . '/default/models/' . $class_name . '.php';

		if(file_exists($require_path))
		{
			require_once $require_path;
			return true;
		}
		return false;
	}

	#Load From Application Folder
	public static function Load_From_Application_Folder( $class_name )
	{
		$class_name = str_replace( '_', '/', $class_name );

		$require_path = APP_PATH . '/' . Light_Router::$module . '/models/' . $class_name . '.php';

		if(file_exists($require_path))
		{
			require_once $require_path;
			return true;
		}
		return false;
	}
}