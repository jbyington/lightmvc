<?php

define("DEFAULT_DATASOURCE", 'default');

#
#	Different DB types have different requirements
#
#	mysql:  type, name, user, pass, addr
#	mssql:  type, name, user, pass, addr
#	mysqli: type, name, user, pass, addr, [port] (default = 3306), [socket]
#	pgsql:  type, name, user, pass, addr, [port] (default = 5432)
#
#
#	Light_Datasources::set
#	( 
#		'default', 
#		array
#		( 
#			'type' => 'mysqli', 
#			'name' => 'default_database', 
#			'user' => 'default_user', 
#			'pass' => 'default_password', 
#			'addr' => 'localhost' 
#		) 
#	);
#
#
#	To get the database reference in the module or application code
#	simply use the following static method
#
#	Light_Datasources::get(); 			Returns the default datasource as 
#										defined in DEFAULT_DATASOURCE.
#
#	Light_Datasources::get('default');	Returns the specified datasource, if 
#										it exists. Otherwise returns false.
#

class Light_Datasources
{
	protected static $dictionary = array();
	public static $defined_datasources = array();

	public static function set( $name = '', $connection_info = array() )
	{
		if( !empty($connection_info) )
		{
			self::$dictionary[$name] = $connection_info;
			self::initialize( $name, $connection_info );
		}
	}
	
	public static function get( $name = '' )
	{
		if( !empty($name) )
		{
			if( isset(self::$defined_datasources[$name]) )
			{
				return self::$defined_datasources[$name];
			}
			return false;
		}
		else
		{
			return self::$defined_datasources['default'];
		}
	}
	
	public static function initialize( $connection_name, $connection_info )
	{
		self::$defined_datasources[$connection_name] = new Light_Database( $connection_info );
		
		if( $connection_name == DEFAULT_DATASOURCE )
		{
			self::$defined_datasources['default'] = new Light_Database( $connection_info );	
		}
	}	
	
	public static function initialize_all()
	{
		foreach( self::$dictionary as $connection_name => $connection_info )
		{			
			self::$initialize( $connection_name, $connection_info );
		}
	}	
	
	public static function destroy( $connection_name )
	{
		self::get($connection_name)->disconnect();
	}
	
	public static function destroy_all()
	{
		foreach( self::$defined_datasources as $connection_name )
		{
			self::destroy($connection_name);
		}
	}
}