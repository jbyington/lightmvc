<?php

class Light_View_Partial extends Light_View_Extension
{
	protected $file_path;
	
	public function __construct( $file_path ) 
	{
		$this->file_path = $file_path;
	}
	
	public function init ( $data = false )
	{
		if( !empty($data) )
		{
			if( is_array($data) )
			{
				foreach( $data as $key => $value )
				{
					$this->$key = $value;
				}
			}
			else if( is_object($data) )
			{
				foreach( get_object_vars($data) as $key => $value )
				{
					$this->$key = $value;
				}
			}
		}
		return $this->get_partial_contents();
	}
	
	protected function get_partial_contents()
	{
		ob_start();			
			include $this->file_path;
			$content = ob_get_contents(); 
		ob_end_clean(); 
		return $content;
	}
	
}
