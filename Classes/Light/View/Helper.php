<?php

class Light_View_Helper
{
	public function __call( $class, $arguments )
	{

		if( !class_exists($class) )
		{

			$class_name = str_replace( '_', '/', $class );

			$path = APP_PATH . '/' . Light_Router::$module . '/views/helpers/' . $class_name . '.php';

			if( file_exists($path) )
			{
				include $path;
			}
		}

		//for helpers, the class name and the method name are the same
		return call_user_func_array( array(new $class, $class), $arguments );
	}
}