<?php
	
class Light_Database_PgSQL
{
	public $last_insert_id = 0;

	private $connection_information;
	private $link = false;

	public function __construct( $conn = array() )
	{		
		$this->connection_information = $conn;
		return $this->link;
	}

	public function connect()
	{		
		if( $this->link === false )
		{
			if( !empty($this->connection_information) )
			{
				$this->link = pg_connect( 'host='.$this->connection_information['addr'].' port='.$this->connection_information['port'].' dbname='.$this->connection_information['name'].' user='.$this->connection_information['user'].' password='.$this->connection_information['pass'] ) or die('Could not connect to Postgres');
				
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	public function disconnect()
	{
		if( $this->link !== false )
		{
			pg_close( $this->link );
			$this->link = false;
		}
		else
		{
			return false;
		}
	}

	public function escape( $string )
	{
		$this->connect();
		return pg_escape_string( $string );
	}
	
	public function quote( $string )
	{
		$this->connect();
		return "'" . pg_escape_string( $string ) . "'";
	}
	
	public function select( $sql )
	{
		return $this->_select( $sql );
	}

	public function selectRow( $sql )
	{
		return reset( $this->_select($sql) );
	}
	
	public function selectOne( $sql )
	{
		return reset( reset( $this->_select($sql) ) );
	}
	
	public function update( $sql )
	{
		return $this->_query( $sql );
	}	

	public function insert( $sql )
	{
		return $this->_query( $sql );
	}
	
	public function delete( $sql )
	{
		return $this->_query( $sql );
	}
	
	public function currentval( $sequence )
	{		
		$value = $this->selectOne( "select currval('$sequence') as value;" );
		
		if( !$value ) 
		{ 
			$value = 0; 
		}

		return $value;
	}

	public function nextval( $sequence )
	{
		return $this->selectOne( "select nextval('$sequence') as value;" );	
	}
	
	//---------------------------------------------------------------------------------
	
	private function _select( $sql )
	{
		$this->connect();
	
		$temp = array();

		if( $results = pg_query( $this->link, $sql ) )
		{
			while( $row = pg_fetch_assoc($results) ) 
			{
				$temp[] = $row;
			}
		}
		
		$this->disconnect();
		
		return $temp;	
	}
	
	private function _query( $sql )
	{
		$this->connect();
	
		$success = false;

		if( $results = pg_query( $this->link, $sql ) )
		{ 
			$success = true; 
		}

		$this->connect(); 
		
		return $success;	
	}	
	
}
