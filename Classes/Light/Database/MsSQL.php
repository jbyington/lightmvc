<?php
	
class Light_Database_MsSQL
{
	public $last_insert_id = 0;

	private $connection_information;
	private $link = false;

	public function __construct( $conn = array() )
	{
		$this->connection_information = $conn;
		return $this->link;
	}
	
	public function connect()
	{
		if( $this->link === false )
		{
			if( !empty($this->connection_information) )
			{
				$this->link = mssql_connect( $this->connection_information['addr'], $this->connection_information['user'], $this->connection_information['pass']) or die('Could not connect to MSSQL Server '.$this->connection_information['addr'] );
				mssql_select_db( $this->connection_information['name'], $this->link );
				
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public function disconnect()
	{
		if( $this->link !== false )
		{
			mssql_close( $this->link );
			$this->link = false; 
		}
		else
		{
			return false;
		}
	}

	public function escape( $string )
	{
		return addslashes($string);
	}
	
	public function quote( $string )
	{
		return "'" . addslashes($string) . "'";
	}
	
	public function select( $sql )
	{
		return $this->_select( $sql );
	}

	public function selectRow( $sql )
	{
		return reset( $this->_select($sql) );
	}
	
	public function selectOne( $sql )
	{
		return reset( reset( $this->_select($sql) ) );
	}
	
	public function update( $sql )
	{
		return $this->_query( $sql );
	}	

	public function insert( $sql )
	{
		return $this->_query( $sql );
	}
	
	public function delete( $sql )
	{
		return $this->_query( $sql );
	}
	
	public function currentval( $sequence )
	{
		return $this->selectOne( "select value from sequences where name = '$sequence'" );
	}

	public function nextval( $sequence )
	{
		$temp = $this->update( "update sequences set value = value + step where name = '$sequence'" );
		return $this->currentval($sequence);
	}
	
	//---------------------------------------------------------------------------------
	
	private function _select( $sql )
	{
		$this->connect();
	
		$temp = array();

		if( $results = mssql_query( $sql, $this->link ) )
		{
			while( $row = mssql_fetch_assoc($results) ) 
			{
				$temp[] = $row;
			}
		}
		
		$this->disconnect();
		
		return $temp;	
	}
	
	private function _query( $sql )
	{
		$this->connect();
	
		$success = false;

		if( $results = mssql_query( $sql, $this->link ) )
		{ 
			$success = true; 
		}

		$this->disconnect();
		
		return $success;	
	}	
	
}