<?php

class Light_Router_Regex
{
	const TOKEN_PATTERN = '/(:[A-Z0-9]+)/i';
	const TOKEN_MATCH_PATTERN = '(:[A-Z0-9]+)';
	const PARAM_PATTERN = '([A-Z0-9-]+)?';
	const SLASH_PATTERN = '(\/?)';
	const NAME_PATTERN = '(\/?)(([A-Z0-9-]+)\.(html|php))?';

	protected $url = '';
	protected $routes = array();
	protected $route_options = array();
	protected $default_params = array(':module' => 'default', ':controller' => 'index', ':action' => 'index', ':id' => false);

	public function __construct(){}
	
	public function route( $url )
	{
		$this->parse_routes($url);

		foreach( $this->route_options as $option ) 
		{
			return $this->prepare_array($option);
		}
		return $this->prepare_array($this->default_params);
	}

	protected function parse_routes( $url )
	{		
		$path = $this->get_path_info($url);
		foreach( $this->routes as $route )
		{
			$regex = $this->create_regex_from_pattern( $route['pattern'] );
			if( $this->pattern_matches( $regex['values'], $url ) )
			{
				$map = $this->create_map( $route['pattern'], $regex['map'], $url, $regex['values']);
				
				if( !empty($path) )
				{
					$map[':name'] = $path['filename'];	
				}
				
				$this->route_options [] = array_merge($route['params'], $map);
			}
		}
	}

	protected function get_path_info( $url )
	{
		$path = pathinfo($url);
	
		if( isset($path['extension']) )
		{
			return array( 'extension' => $path['extension'], 'filename' => $path['filename'] );
		}
		
		return array();
	}

	protected function prepare_array( $array )
	{
		$return = array();
		foreach( $array as $key => $value )
		{
			$return[ str_replace(':','',$key) ] = str_replace( '-', '_', $value );
		}
		return $return;
	}

	public function add( $name, $pattern, $params = array() )
	{		
		$this->routes[$name] = array('pattern' => $pattern, 'params' => array_merge( $this->default_params, $params ) );
		return $this;
	}

	public function create_regex_from_pattern( $pattern )
	{
		$pattern = str_replace('/', self::SLASH_PATTERN, $pattern);
		return array
		( 
			'map' => '/^'.preg_filter( self::TOKEN_PATTERN, self::TOKEN_MATCH_PATTERN, $pattern) . self::NAME_PATTERN . '/i',
			'values' => '/^'.preg_filter( self::TOKEN_PATTERN, self::PARAM_PATTERN, $pattern) . self::NAME_PATTERN . '/i'
		);
	}

	public function pattern_matches( $pattern, $url )
	{
		return preg_match( $pattern, $url );
	}

	public function create_map( $map_string, $map_regex, $value_string, $value_regex )
	{
		$map = $this->create_match_array( $map_string, $map_regex );
		$values = $this->create_match_array( $value_string, $value_regex );

		$return = $this->zip_arrays( $map, $values );
		
		return $this->clean_up($return);
	}

	protected function clean_up( $array = array() )
	{
		foreach( $array as $k => $v )
		{
			if( strpos($k,'/') !== false || empty($v) )
			{
				unset($array[$k]);
			}
		}
		return $array;
	}

	protected function zip_arrays( $map, $values )
	{
		$return = array();
		if( !empty($map) && !empty($values) )
		{
			foreach( $map as $k => $v )
			{
				#arrays will have unequal number of indexes. 
				#Array_Combine doesnt work here
				$return[$v] = $values[$k];
			}
		}
		return $return;
	}

	public function create_match_array( $string, $regex )
	{
		$params = array();
		preg_match_all( $regex, $string, $params, PREG_SET_ORDER );
		return reset($params);
	}
	
}