<?php

#
#	Define Light_Environment singleton values to use in other classes
# 	bool Light_Environment::set( str $Environment, str $Url );
# 	bool Light_Environment::set_multiple( array $Environments );
#
#	Example:
#	Light_Environment::set('local', 'local.lightmvc.com');
#	Light_Environment::set('qa', 'qa.lightmvc.com');
#	Light_Environment::set('prod', 'www.lightmvc.com');
#
#
#	Call with is()
# 	bool Light_Environment::is( str $Environment );
#
#	This is helpful if you want to enable/disable debugging
#	by current development environment;
#
#	ex. Light_Environment::is('local');
#
#
#	Call with current()
# 	string Light_Environment::current();
#
#	ex. $env = Light_Environment::current();
#

define('LIGHT_ENVIRONMENT', Light_Environment::current());

class Light_Environment
{		
	protected static $environments = array();
			
	public static function is( $environment )
	{			
		if( self::$environments[$environment] == $_SERVER['SERVER_NAME'] )
		{
			return true;
		}
		return false;
	}
	
	public static function set( $name = '', $url = '' )
	{
		if( empty($name) || empty($url) )
		{
			return false;
		}
		else
		{
			self::$environments[$name] = $url;
		}		
	}

	public static function set_multiple( $environments )
	{
		foreach( $environments as $key => $value )
		{
			self::$set( $key, $value );
		}
	}
	
	public static function current()
	{
		foreach( self::$environments as $environment => $domain )
		{
			if( self::is( $environment ) )
			{
				return $environment;
			}
		}
	}
}