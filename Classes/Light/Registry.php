<?php
	
class Light_Registry
{		
	protected static $registry = array();
			
	public static function get( $key )
	{			
		if( isset(self::$registry[$key] ) )
		{
			return self::$registry[$key];
		}
		return false;
	}
	
	public static function set( $key = '', $value = '' )
	{
		if( self::$registry[$key] = $value )
		{
			return true;
		}
		else
		{
			return false;
		}		
	}

	public static function set_multiple( $array_of_keys_and_values )
	{
		foreach( $array_of_keys_and_values as $key => $value )
		{
			self::$set( $key, $value );
		}
	}
	
}